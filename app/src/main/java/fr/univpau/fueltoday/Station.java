package fr.univpau.fueltoday;

import java.io.Serializable;

/**
 * Copyright (c) 2024 Clara Caussé et Mathilde Cholley.
 */
public class Station implements Serializable {
    private String address;
    private double latitude;
    private double longitude;
    private double distance; // Ajout de l'attribut de distance
    private double gazole_prix;
    private double sp95_prix;
    private double e85_prix;
    private double gplc_prix;
    private double e10_prix;
    private double sp98_prix;
    private String services;

    // Modifiez le constructeur pour initialiser la liste de services
    public Station(String address, double latitude, double longitude, double gazole_prix, double sp95_prix, double e85_prix, double gplc_prix, double e10_prix, double sp98_prix,String services) {
        this.address = address;
        this.latitude = latitude;
        this.longitude = longitude;
        this.distance = 0.0;
        this.services = services;
        this.gazole_prix = gazole_prix;
        this.sp95_prix = sp95_prix;
        this.e85_prix = e85_prix;
        this.gplc_prix = gplc_prix;
        this.e10_prix = e10_prix;
        this.sp98_prix = sp98_prix;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public double getGazole_prix() {
        return gazole_prix;
    }

    public void setGazole_prix(double gazole_prix) {
        this.gazole_prix = gazole_prix;
    }

    public double getSp95_prix() {
        return sp95_prix;
    }

    public void setSp95_prix(double sp95_prix) {
        this.sp95_prix = sp95_prix;
    }

    public double getE85_prix() {
        return e85_prix;
    }

    public void setE85_prix(double e85_prix) {
        this.e85_prix = e85_prix;
    }

    public double getGplc_prix() {
        return gplc_prix;
    }

    public void setGplc_prix(double gplc_prix) {
        this.gplc_prix = gplc_prix;
    }

    public double getE10_prix() {
        return e10_prix;
    }

    public void setE10_prix(double e10_prix) {
        this.e10_prix = e10_prix;
    }

    public double getSp98_prix() {
        return sp98_prix;
    }

    public void setSp98_prix(double sp98_prix) {
        this.sp98_prix = sp98_prix;
    }

    public String getServices() {
        return services;
    }

    /*public void setServices(String services) {
        this.services = services;
    }*/
}
