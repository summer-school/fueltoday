package fr.univpau.fueltoday;
/**
 * Copyright (c) 2024 Clara Caussé et Mathilde Cholley.
 */
import android.content.Intent;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.drawable.ShapeDrawable;
import android.net.Uri;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.Gravity;
import android.view.View;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.TextView;

import androidx.appcompat.app.AppCompatActivity;
import androidx.cardview.widget.CardView;
import androidx.constraintlayout.widget.ConstraintHelper;
import androidx.core.view.ViewCompat;

import com.google.android.material.shape.CornerFamily;
import com.google.android.material.shape.MaterialShapeDrawable;
import com.google.android.material.shape.ShapeAppearanceModel;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DecimalFormat;

public class StationActivity extends AppCompatActivity {


    private LinearLayout buttonContainer;
    private LinearLayout buttonImages;

    private double gazole_prix;
    private double sp95_prix;
    private double e85_prix;
    private double gplc_prix;
    private double e10_prix;
    private double sp98_prix;

    // ...

    // ...

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.station_activity);

        // Retrieve data from Intent
        Intent intent = getIntent();
        if (intent != null) {
            // Retrieve the Station object
            buttonContainer = findViewById(R.id.buttonContainer);
            buttonImages = findViewById(R.id.buttonImages);

            Station selectedStation = (Station) intent.getSerializableExtra("selectedStation");

            // Use the Station object as needed
            if (selectedStation != null) {


                // Array of image resources
                int[] imageResources = {
                        R.drawable.gazole,
                        R.drawable.sp98,
                        R.drawable.e85,
                        R.drawable.e10,
                        R.drawable.sp95,
                        R.drawable.gplc
                        // Add more image resources if needed
                };


                // Access properties of the selectedStation
                String address = selectedStation.getAddress();
                double distance = selectedStation.getDistance();

                Button stationButton = new Button(this);
                stationButton.setText(selectedStation.getAddress());
                stationButton.setTextColor(Color.WHITE);

                MaterialShapeDrawable shapeDrawable = new MaterialShapeDrawable();
                int lightGrayColor = Color.parseColor("#605c5b");
                shapeDrawable.setFillColor(ColorStateList.valueOf(lightGrayColor)); // Use ColorStateList
                shapeDrawable.setShapeAppearanceModel(
                        ShapeAppearanceModel.builder()
                                .setAllCorners(CornerFamily.ROUNDED, 25.0f)
                                .build()
                );

                ViewCompat.setBackground(stationButton, shapeDrawable);

                // Set layout parameters for the button
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT, // Use MATCH_PARENT for width
                        LinearLayout.LayoutParams.WRAP_CONTENT
                );

                // Adjust the margins as needed
                params.setMargins(16, 8, 16, 8);

                stationButton.setLayoutParams(params);

                stationButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        // Handle button click
                    }
                });

                // Add the button to the LinearLayout
                buttonContainer.addView(stationButton);

                // Créer un champ de texte (TextView)
                TextView textField = new TextView(this);
                textField.setText("0.000 -> Carburant Non Disponible"); // Ajouter le texte au TextView

// Changer la couleur du texte en #fbcc92
                int textColor = Color.parseColor("#f3603a");
                textField.setTextColor(textColor);

// Ajouter le champ de texte à votre layout
                buttonContainer.addView(textField);


// Ajout de l'adresse et de la distance formatée à la chaîne d'informations de la station
                StringBuilder stationInfo = new StringBuilder();


// Afficher les informations dans le TextView
                TextView stationInfoTextView = findViewById(R.id.stationInfoTextView);
                stationInfoTextView.setText(stationInfo.toString());


                gazole_prix = selectedStation.getGazole_prix();
                sp95_prix = selectedStation.getSp95_prix();
                e85_prix = selectedStation.getE85_prix();
                gplc_prix = selectedStation.getGplc_prix();
                e10_prix = selectedStation.getE10_prix();
                sp98_prix = selectedStation.getSp98_prix();

                Log.d("PRIX", "Gazole Prix: " + gazole_prix);
                Log.d("PRIX", "SP95 Prix: " + sp95_prix);
                Log.d("PRIX", "E85 Prix: " + e85_prix);
                Log.d("PRIX", "GPLC Prix: " + gplc_prix);
                Log.d("PRIX", "E10 Prix: " + e10_prix);
                Log.d("PRIX", "SP98 Prix: " + sp98_prix);

                generateImageButtons();
                // Append services
                String servicesJsonString = selectedStation.getServices();
                if (servicesJsonString != null && !servicesJsonString.isEmpty()) {
                    try {
                        // Convert the services string to a JSONObject
                        JSONObject servicesObject = new JSONObject(servicesJsonString);

                        // Check if the "services" key exists
                        if (servicesObject.has("service")) {
                            // Extract the services array
                            JSONArray servicesArray = servicesObject.getJSONArray("service");

                            // Append services to the station info
                            appendServices(stationInfo, servicesArray);
                        }

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else {
                    ///
                }


                generateMenuButton();
                generateGoogleMapsButton(address);
            }

        }
    }


    // ...
    private void generateGoogleMapsButton(String address) {
        // Créer un bouton pour ouvrir Google Maps
        Button btnGoogleMaps = new Button(this);
        btnGoogleMaps.setId(ViewCompat.generateViewId());
        btnGoogleMaps.setText("TRAJET");
        btnGoogleMaps.setTextColor(Color.WHITE);

        // Create a MaterialShapeDrawable for the button background
        MaterialShapeDrawable shapeDrawable = new MaterialShapeDrawable();
        int orangeColor = Color.parseColor("#fbcc92"); // Orange color
        shapeDrawable.setFillColor(ColorStateList.valueOf(orangeColor)); // Use ColorStateList
        shapeDrawable.setShapeAppearanceModel(
                ShapeAppearanceModel.builder()
                        .setAllCorners(CornerFamily.ROUNDED, 25.0f) // Utilisez un rayon suffisamment grand pour obtenir un cercle
                        .build()
        );

        // Set the background drawable for the button
        ViewCompat.setBackground(btnGoogleMaps, shapeDrawable);

        btnGoogleMaps.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);

        // Set layout parameters for the button
        LinearLayout.LayoutParams btnGoogleMapsParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
        );

        // Adjust the margins as needed
        btnGoogleMapsParams.setMargins(16, 16, 16, 16);
        btnGoogleMaps.setLayoutParams(btnGoogleMapsParams);

        // Définir un écouteur de clic pour le bouton Google Maps
        btnGoogleMaps.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Ouvrir Google Maps avec l'adresse spécifiée
                openGoogleMaps(address);
            }
        });

        // Ajouter le bouton Google Maps au layout principal
        LinearLayout mainLayout = findViewById(R.id.buttonImages2); // Changer à votre ID de layout principal
        mainLayout.addView(btnGoogleMaps);
    }

    private void openGoogleMaps(String address) {
        // Créer une Uri avec l'adresse pour l'intention Google Maps
        Uri gmmIntentUri = Uri.parse("geo:0,0?q=" + Uri.encode(address));

        // Créer une intention pour ouvrir Google Maps avec l'adresse spécifiée
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
        mapIntent.setPackage("com.google.android.apps.maps");

        // Vérifier si l'application Google Maps est installée
        if (mapIntent.resolveActivity(getPackageManager()) != null) {
            startActivity(mapIntent);
        }
    }

    private void generateMenuButton() {
        // Create a Button for the menu
        Button redBtnMenu = new Button(this);
        redBtnMenu.setId(ViewCompat.generateViewId());
        redBtnMenu.setText("MENU");
        redBtnMenu.setTextColor(Color.WHITE);

        // Create a MaterialShapeDrawable for the button background
        MaterialShapeDrawable shapeDrawable = new MaterialShapeDrawable();
        int redColor = Color.parseColor("#fbcc92"); // Red color
        shapeDrawable.setFillColor(ColorStateList.valueOf(redColor)); // Use ColorStateList
        shapeDrawable.setShapeAppearanceModel(
                ShapeAppearanceModel.builder()
                        .setAllCorners(CornerFamily.ROUNDED, 25.0f)
                        .build()
        );

        // Set the background drawable for the button
        ViewCompat.setBackground(redBtnMenu, shapeDrawable);

        redBtnMenu.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);

        // Set layout parameters for the button
        // ...

// Set layout parameters for the button
        LinearLayout.LayoutParams redBtnMenuParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
        );

// Adjust the margins as needed
        redBtnMenuParams.setMargins(170, 16, 200, 50);
        redBtnMenu.setLayoutParams(redBtnMenuParams);

// Set padding for the button
        int paddingInDp = 8;  // Adjust the value of padding as needed
        float scale = getResources().getDisplayMetrics().density;
        int paddingInPixels = (int) (paddingInDp * scale + 0.5f);
        redBtnMenu.setPadding(paddingInPixels, paddingInPixels, paddingInPixels, paddingInPixels);

// Set click listener for the menu button
        redBtnMenu.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Handle menu button click
                // You can open a menu, navigate to another activity, etc.
                // For example, you can show a Toast message for now:
                finish();
            }
        });

// Add the red menu button to the main layout
        LinearLayout mainLayout = findViewById(R.id.buttonImages2); // Change to your main layout ID
        mainLayout.addView(redBtnMenu);

// ...


        // Add the red menu button to the main layout

    }

    private void appendServices(StringBuilder stationInfo, JSONArray servicesArray) {
        // Create a new CardView for services
        CardView servicesCardView = new CardView(this);
        servicesCardView.setRadius(25.0f); // Ajustez le rayon des coins selon vos besoins
        servicesCardView.setCardBackgroundColor(Color.parseColor("#605c5b")); // Change background color as needed

        // Set margin top to the services card view
        LinearLayout.LayoutParams servicesCardParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
        );
        servicesCardParams.setMargins(0, 50, 0, 0); // Adjust the top margin as needed
        servicesCardView.setLayoutParams(servicesCardParams);

        // Create a new LinearLayout for services inside the CardView
        LinearLayout servicesLayout = new LinearLayout(this);
        servicesLayout.setOrientation(LinearLayout.VERTICAL);

        TextView titleTextView = new TextView(this);
        titleTextView.setText("SERVICES DISPONIBLES");
        titleTextView.setTextColor(Color.WHITE);
        titleTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18); // Adjust text size as needed
        titleTextView.setGravity(Gravity.START); // Adjust gravity as needed

        // Add the title TextView to the services layout
        servicesLayout.addView(titleTextView);
        // Iterate through the services and append them to the station info
        for (int i = 0; i < servicesArray.length(); i++) {
            try {
                String service = servicesArray.getString(i);

                // Create a TextView for each service
                TextView serviceTextView = new TextView(this);
                serviceTextView.setText(service);

                // Set additional properties for the TextView
                serviceTextView.setTextColor(Color.WHITE);
                serviceTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 14); // Adjust text size as needed

                // Add the TextView to the services layout
                servicesLayout.addView(serviceTextView);

                // Append a comma if it's not the last service
                if (i < servicesArray.length() - 1) {
                    stationInfo.append(", ");
                }

            } catch (JSONException e) {
                e.printStackTrace();
            }
        }

        // Add the services layout to the CardView
        servicesCardView.addView(servicesLayout);

        // Add the CardView to the main layout (buttonImages)
        buttonImages.addView(servicesCardView);
    }


    // Helper method to append fuel price if not null or zero
    private void appendFuelPrice(StringBuilder stationInfo, String fuelType, double price) {
        if (price != 0.0) {
            //stationInfo.append(fuelType).append(price).append(" €").append("\n");
        } else {

        }
    }

    private void generateImageButtons() {
        // Créer un conteneur vertical pour le texte et les boutons
        CardView container = new CardView(this);
        container.setRadius(25.0f); // Ajustez le rayon des coins selon vos besoins
        container.setCardBackgroundColor(Color.parseColor("#605c5b")); // Change background color as needed

        // Ajouter le conteneur au conteneur principal
        buttonImages.addView(container);

        // Ajouter le texte "Carburant:" au-dessus des boutons
        TextView carburantsTextView = new TextView(this);
        carburantsTextView.setText("CARBURANTS");
        carburantsTextView.setTextColor(Color.WHITE);
        carburantsTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 18);
        carburantsTextView.setGravity(Gravity.START);

// Ajouter un padding au TextView
        // Ajouter un padding au TextView
        int paddingInDp = 16;  // Ajustez la valeur du padding selon vos besoins
        float scale = getResources().getDisplayMetrics().density;
        int paddingInPixels = (int) (paddingInDp * scale + 0.5f);
        carburantsTextView.setPadding(paddingInPixels, 0, 0, 50);  // Ajustez le dernier argument pour le padding du bas

// Ajouter le TextView au conteneur
        LinearLayout.LayoutParams carburantsParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
        );
        carburantsParams.gravity = Gravity.START;
        container.addView(carburantsTextView, carburantsParams);

        // Créer un conteneur vertical pour les boutons
        LinearLayout buttonContainer = new LinearLayout(this);
        buttonContainer.setOrientation(LinearLayout.VERTICAL);

        // Ajouter le conteneur de boutons au conteneur principal
        container.addView(buttonContainer);

        // Créer trois lignes pour les boutons
        LinearLayout firstRow = new LinearLayout(this);
        LinearLayout secondRow = new LinearLayout(this);
        LinearLayout thirdRow = new LinearLayout(this);

        // Paramètres de mise en page pour les lignes
        LinearLayout.LayoutParams rowParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.MATCH_PARENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
        );

        // Ajouter les lignes au conteneur des boutons
        buttonContainer.addView(firstRow, rowParams);
        buttonContainer.addView(secondRow, rowParams);
        buttonContainer.addView(thirdRow, rowParams);

        // Générer les trois premiers boutons et les ajouter à la première ligne
        generateImageButton(firstRow, R.drawable.gazole, gazole_prix);
        generateImageButton(firstRow, R.drawable.sp98, sp98_prix);

        // Générer les trois suivants et les ajouter à la deuxième ligne
        generateImageButton(secondRow, R.drawable.e85, e85_prix);
        generateImageButton(secondRow, R.drawable.e10, e10_prix);

        // Générer les trois derniers et les ajouter à la troisième ligne
        generateImageButton(thirdRow, R.drawable.sp95, sp95_prix);
        generateImageButton(thirdRow, R.drawable.gplc, gplc_prix);

        Log.d("PRIXTEST", "Gazole Prix: " + String.valueOf(gazole_prix));
    }


    private void generateImageButton(LinearLayout row, int imageResource, double legend) {
        ImageButton imageButton = new ImageButton(this);
        imageButton.setId(ViewCompat.generateViewId());
        int buttonHeight = 0;
        double buttonWidth = 0;
        int buttonWidthInt = 0;

        // Obtenir les dimensions souhaitées en fonction de la résolution de l'écran
        DisplayMetrics display = getResources().getDisplayMetrics();
        buttonHeight = (int) (display.heightPixels / 1.5);
        buttonWidth = buttonHeight * 2.66666667;
        buttonWidthInt = (int) Math.round(buttonWidth);

        // Définir une largeur maximale plus basse (par exemple, un quart de l'écran)
        int maxWidth = (int) (display.widthPixels / 3.2);  // Ajustez cela en fonction de vos besoins
        if (buttonWidthInt > maxWidth) {
            buttonWidthInt = maxWidth;
            // Recalculer la hauteur en fonction de la nouvelle largeur
            buttonHeight = (int) Math.round(buttonWidthInt / 2.66666667);
        }


        // Définir les paramètres de mise en page
        LinearLayout.LayoutParams buttonParams = new LinearLayout.LayoutParams(buttonWidthInt, buttonHeight);
        imageButton.setLayoutParams(buttonParams);
        imageButton.setScaleType(ImageButton.ScaleType.CENTER_INSIDE);

        // Définir l'image de ressource
        imageButton.setImageResource(imageResource);

        // Définir d'autres attributs si nécessaire
        imageButton.setBackgroundColor(Color.TRANSPARENT);

        imageButton.setPadding(2, 40, 2, 20);  // Ajustez ces valeurs selon vos besoins
        buttonParams.setMargins(2, 40, 2, 20);

        // Ajouter le bouton à la ligne spécifiée
        row.addView(imageButton);

        // Ajouter la légende sous le bouton

// Ajouter la légende sous le bouton
        TextView legendTextView = new TextView(this);
        if (legend == 0.0) {
            // legendTextView.setText("Prix non disponible");
            legendTextView.setText("0.000 €");
            // legendTextView.setTextColor(Color.RED);
            int legendTextColor = Color.parseColor("#f3603a");

// Appliquer la couleur au TextView
            legendTextView.setTextColor(legendTextColor);
        } else {
            String formattedPrice = String.format("%.3f", legend);
            legendTextView.setText(formattedPrice + " €");
            legendTextView.setTextColor(Color.WHITE);
        }

        legendTextView.setTextSize(TypedValue.COMPLEX_UNIT_SP, 10);  // Ajustez la taille du texte selon vos besoins
        legendTextView.setGravity(Gravity.CENTER);

// Définir les paramètres de mise en page pour la légende
        LinearLayout.LayoutParams legendParams = new LinearLayout.LayoutParams(
                LinearLayout.LayoutParams.WRAP_CONTENT,
                LinearLayout.LayoutParams.WRAP_CONTENT
        );
        legendParams.gravity = Gravity.CENTER;
        legendParams.setMargins(2, 20, 2, 2); // Ajustez ces valeurs selon vos besoins

// Ajouter un padding à la légende
        legendTextView.setPadding(2, 10, 2, 2); // Ajustez ces valeurs selon vos besoins

// Ajouter la légende à la ligne spécifiée
        row.addView(legendTextView, legendParams);


    }


}

// ...
