package fr.univpau.fueltoday;

import static android.Manifest.permission.ACCESS_BACKGROUND_LOCATION;
import static android.Manifest.permission.ACCESS_FINE_LOCATION;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.app.ActivityCompat;
import androidx.core.content.ContextCompat;
import androidx.core.view.ViewCompat;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentTransaction;
import androidx.swiperefreshlayout.widget.SwipeRefreshLayout;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentSender;
import android.content.pm.PackageManager;
import android.content.res.ColorStateList;
import android.graphics.Color;
import android.graphics.Typeface;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.text.Spannable;
import android.text.SpannableString;
import android.text.style.StyleSpan;
import android.util.DisplayMetrics;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.ArrayAdapter;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageButton;
import android.widget.LinearLayout;
import android.widget.ProgressBar;
import android.widget.SeekBar;
import android.widget.Spinner;
import android.widget.TextView;

import com.google.android.gms.common.api.ResolvableApiException;
import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.location.LocationSettingsRequest;
import com.google.android.gms.location.LocationSettingsResponse;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.tasks.OnFailureListener;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.android.gms.tasks.Task;
import com.google.android.material.shape.CornerFamily;
import com.google.android.material.shape.MaterialShapeDrawable;
import com.google.android.material.shape.ShapeAppearanceModel;
import com.google.android.material.tabs.TabLayout;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.URL;
import java.text.DecimalFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

import android.location.Address;
import android.location.Geocoder;


/**
 * Copyright (c) 2024 Clara Caussé et Mathilde Cholley.
 */
public class MainActivity extends AppCompatActivity {
    private static final int LOCATION_SETTINGS_REQUEST_CODE = 1001;

    private Location location;

    private Button quitButton;

    private Button refreshButton;


    private ProgressDialog progressDialog;
    private ImageButton settingsButton;
    private List<Station> stationList = new ArrayList<>();
    private static final int LOCATION_PERMISSION_REQUEST_CODE = 123;
    private static final int BACKGROUND_LOCATION_PERMISSION_REQUEST_CODE = 456;


    private static String result;
    private ProgressBar progressBar;
    private double currentLongitude;
    private double currentLatitude;
    //private static final int LOCATION_PERMISSION_REQUEST_CODE = 1;
    // private static final String apiUrl = "https://data.economie.gouv.fr/api/explore/v2.1/catalog/datasets/prix-des-carburants-en-france-flux-instantane-v2/records?where=within_distance(geom%2C%20GEOM%27POINT(2.3522%2048.8566)%27%2C%2020km)";
    private TextView textView;
    private FusedLocationProviderClient fusedLocationClient;

    private TabLayout tabLayout;
    private LocationManager locationManager;
    private LocationListener locationListener;
    private SwipeRefreshLayout swipeRefreshLayout;
    private int selectedTabPosition = 0;
    // private String[] allServicesArray = {"Toilettes publiques", "Relais colis", "Boutique alimentaire", "Boutique non alimentaire", "Restauration sur place", "Location de véhicule", "Lavage automatique", "Vente de gaz domestique (Butane, Propane)", "Automate CB 24/24", "DAB (Distributeur automatique de billets)"};
    private String[] allServicesArray = {
            "Toilettes publiques",
            "Relais colis",
            "Boutique alimentaire",
            "Boutique non alimentaire",
            "Restauration sur place",
            "Location de véhicule",
            "Lavage automatique",
            "Vente de gaz domestique (Butane, Propane)",
            "Automate CB 24/24",
            "DAB (Distributeur automatique de billets)",
            "Automate 24-24 (oui/non)",
            "Station de gonflage",
            "Piste poids lourds",
            "Carburant additivé",
            "Lavage manuel",
            "Restauration à emporter",
            "Relais colis",
            "Laverie",
            "Vente d'additifs carburants",
            "Services réparation / entretien",
            "Wifi",
            "Vente de fioul domestique",
            "Vente de pétrole lampant",
            "Bornes électriques",
            "Bar",
            "Espace bébé",
            "Douches",
            "Aire de camping-cars",
            "GNV"
    };

    private List<String> selectedServicesList = new ArrayList<>();


    private String rayon = "20";
    private String preference_carburant = "";
    private LinearLayout buttonContainer;
    private String updatedUrl;

    private String fileName = "preference_gazoil_now.json";
    private static final String BASE_API_URL = "https://data.economie.gouv.fr/api/explore/v2.1/catalog/datasets/prix-des-carburants-en-france-flux-instantane-v2/records?where=within_distance(geom%2C%20GEOM%27POINT(<longitude>%20<latitude>)%27%2C%20<rayon>km)";
    private File jsonFile;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        getWindow().getAttributes().screenBrightness = WindowManager.LayoutParams.BRIGHTNESS_OVERRIDE_FULL;

        Toolbar toolbar = findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        // Titre en gras
        SpannableString spannableString = new SpannableString(toolbar.getTitle());
        spannableString.setSpan(new StyleSpan(Typeface.BOLD), 0, spannableString.length(), Spannable.SPAN_EXCLUSIVE_EXCLUSIVE);
        toolbar.setTitle(spannableString);
        progressBar = findViewById(R.id.progressBar);
        textView = findViewById(R.id.textView);
        buttonContainer = findViewById(R.id.buttonContainer);

// Obtenez une référence à votre TabLayout
        tabLayout = findViewById(R.id.tableLayout);

// Personnalisez l'indicateur de l'onglet sélectionné
        tabLayout.setSelectedTabIndicatorColor(Color.parseColor("#44b1d0"));

        jsonFile = new File(getFilesDir(), fileName);

        try {
            if (!jsonFile.exists()) {
                // Le fichier n'existe pas, alors créez-le avec les valeurs par défaut
                createDefaultJsonFile(jsonFile);
                showSettingsDialog2();
            } else {
                // Le fichier existe, donc lisez les valeurs à partir du fichier
                readJsonFile(jsonFile);
            }
        } catch (IOException | JSONException e) {
            e.printStackTrace();
        }


// Définissez la couleur de fond de TabLayout
        tabLayout.setBackgroundColor(Color.parseColor("#363636"));

        settingsButton = findViewById(R.id.settingsButton);
        settingsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                showSettingsDialog();
            }
        });

        quitButton = findViewById(R.id.quitButton);
        quitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Appeler la méthode finish() pour fermer l'activité
                finish();
            }
        });

        refreshButton = findViewById(R.id.refreshButton);
        refreshButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                // Appeler la méthode finish() pour fermer l'activité
                refreshData();
            }
        });


// Définissez les couleurs du texte des onglets (non sélectionné et sélectionné)
        tabLayout.setTabTextColors(Color.parseColor("#FFFFFF"), Color.parseColor("#44b1d0"));


        tabLayout.addOnTabSelectedListener(new TabLayout.OnTabSelectedListener() {
            @Override
            public void onTabSelected(TabLayout.Tab tab) {
                // Mettez à jour le contenu du TextView en fonction de l'onglet sélectionné
                selectedTabPosition = tab.getPosition();
                updateTextViewContent(tab.getPosition());
            }

            @Override
            public void onTabUnselected(TabLayout.Tab tab) {
                // Vous pouvez gérer quelque chose si besoin lors du changement d'onglet
            }

            @Override
            public void onTabReselected(TabLayout.Tab tab) {
                // Vous pouvez gérer quelque chose si besoin lors de la sélection répétée de l'onglet
            }
        });

        locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);

        locationListener = new LocationListener() {
            @Override
            public void onLocationChanged(Location location) {


            }


            @Override
            public void onStatusChanged(String provider, int status, Bundle extras) {
            }

            @Override
            public void onProviderEnabled(String provider) {
            }

            @Override
            public void onProviderDisabled(String provider) {
            }
        };


        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        List<String> providers = locationManager.getProviders(true);
        Log.d("Location", "Providers: " + providers);

        //requestLocationSettings();

        if (ContextCompat.checkSelfPermission(this, ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(this, new String[]{ACCESS_FINE_LOCATION}, LOCATION_PERMISSION_REQUEST_CODE);

        } else {
            FusedLocationProviderClient mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
            mFusedLocationClient.getLastLocation()
                    .addOnSuccessListener(this, new OnSuccessListener<Location>() {
                        @Override
                        public void onSuccess(Location location) {
                            // La localisation GPS peut être nulle si le GPS est désactivé
                            if (location != null) {
                                Log.d("Location", "aaaaaaaaa");
                                double latitude = location.getLatitude();
                                double longitude = location.getLongitude();
                                Log.d("Location", "Latitude: " + latitude + ", Longitude: " + longitude);

                                String updatedUrl = BASE_API_URL
                                        .replace("<longitude>", isValidDoubleString(longitude) ? String.valueOf(longitude) : "0.0")
                                        .replace("<latitude>", isValidDoubleString(latitude) ? String.valueOf(latitude) : "0.0")
                                        .replace("<rayon>", rayon);
                                Log.d("Location", "Latitude: " + updatedUrl);


                                // Utilisez l'URL mise à jour pour la tâche de récupération des données
                                currentLatitude = latitude;
                                currentLongitude = longitude;
                                new FetchDataTask().execute(updatedUrl);
                            } else
                                Log.d("Location", "Erreur lors de la tentative d'obtention de la dernière localisation GPS");

                        }
                    })
                    .addOnFailureListener(this, new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Log.d("MapDemoActivity", "Erreur lors de la tentative d'obtention de la dernière localisation GPS");
                            e.printStackTrace();
                        }
                    });
        }


    }


    private void createDefaultJsonFile(File jsonFile) throws IOException, JSONException {
        jsonFile.createNewFile();

        // Créer un objet JSON avec les valeurs par défaut
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("rayon", rayon);
        jsonObject.put("preference_carburant", preference_carburant);
        jsonObject.put("stationList", new JSONArray());

        // Écrire l'objet JSON dans le fichier
        FileWriter fileWriter = new FileWriter(jsonFile);
        fileWriter.write(jsonObject.toString());
        fileWriter.close();

    }

    private void readJsonFile(File jsonFile) throws IOException, JSONException {
        // Lire le contenu du fichier JSON
        BufferedReader bufferedReader = new BufferedReader(new FileReader(jsonFile));
        StringBuilder stringBuilder = new StringBuilder();
        String line;
        while ((line = bufferedReader.readLine()) != null) {
            stringBuilder.append(line).append('\n');
        }
        bufferedReader.close();

        // Convertir la chaîne JSON en objet JSON
        JSONObject jsonObject = new JSONObject(stringBuilder.toString());

        // Extraire les valeurs du fichier JSON et mettre à jour les variables de classe
        rayon = jsonObject.getString("rayon");
        preference_carburant = jsonObject.getString("preference_carburant");

        JSONArray servicesArray = jsonObject.getJSONArray("selectedServicesList");
        selectedServicesList.clear();
        for (int i = 0; i < servicesArray.length(); i++) {
            selectedServicesList.add(servicesArray.getString(i));
        }
    }

    private void writeJsonFile(File jsonFile) throws IOException, JSONException {
        // Créer un objet JSON avec les valeurs actuelles
        JSONObject jsonObject = new JSONObject();
        jsonObject.put("rayon", rayon);
        jsonObject.put("preference_carburant", preference_carburant);

        // Convertir la liste de services en un tableau JSON
        JSONArray servicesArray = new JSONArray();
        for (String service : selectedServicesList) {
            servicesArray.put(service);
        }
        jsonObject.put("selectedServicesList", servicesArray);

        // Écrire l'objet JSON dans le fichier
        FileWriter fileWriter = new FileWriter(jsonFile);
        fileWriter.write(jsonObject.toString());
        fileWriter.close();
    }


    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        if (requestCode == LOCATION_PERMISSION_REQUEST_CODE) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // La permission a été accordée. Vous pouvez effectuer des opérations liées à la localisation ici.
                // Par exemple, appeler la méthode getLastLocationNewMethod() ici.
                //getLastLocationNewMethod();
                if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                    FusedLocationProviderClient mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
                    mFusedLocationClient.getLastLocation()
                            .addOnSuccessListener(this, new OnSuccessListener<Location>() {
                                @Override
                                public void onSuccess(Location location) {
                                    // La localisation GPS peut être nulle si le GPS est désactivé
                                    if (location != null) {
                                        Log.d("Location", "aaaaaaaaa");
                                        double latitude = location.getLatitude();
                                        double longitude = location.getLongitude();
                                        Log.d("Location", "Latitude: " + latitude + ", Longitude: " + longitude);

                                        String updatedUrl = BASE_API_URL
                                                .replace("<longitude>", isValidDoubleString(longitude) ? String.valueOf(longitude) : "0.0")
                                                .replace("<latitude>", isValidDoubleString(latitude) ? String.valueOf(latitude) : "0.0")
                                                .replace("<rayon>", rayon);
                                        Log.d("Location", "Latitude: " + updatedUrl);


                                        // Utilisez l'URL mise à jour pour la tâche de récupération des données
                                        currentLatitude = latitude;
                                        currentLongitude = longitude;
                                        new FetchDataTask().execute(updatedUrl);
                                    } else
                                        Log.d("Location", "Erreur lors de la tentative d'obtention de la dernière localisation GPS");

                                }
                            })
                            .addOnFailureListener(this, new OnFailureListener() {
                                @Override
                                public void onFailure(@NonNull Exception e) {
                                    Log.d("MapDemoActivity", "Erreur lors de la tentative d'obtention de la dernière localisation GPS");
                                    e.printStackTrace();
                                }
                            });
                }
            } else {
                // La permission a été refusée. Gérez cela en conséquence.
                Log.d("Location", "La permission a été refusée.");
            }
        }
    }

    private void requestLocationSettings() {
        LocationSettingsRequest.Builder builder = new LocationSettingsRequest.Builder().addLocationRequest(new LocationRequest());
        Task<LocationSettingsResponse> result = LocationServices.getSettingsClient(this).checkLocationSettings(builder.build());

        result.addOnSuccessListener(locationSettingsResponse -> {
            // Les paramètres de localisation sont déjà configurés correctement.
            // Continuez avec l'opération de localisation.
            // getLastKnownLocation();
        });

        result.addOnFailureListener(e -> {
            if (e instanceof ResolvableApiException) {
                try {
                    // Afficher le dialogue pour activer les paramètres de localisation.
                    ResolvableApiException resolvable = (ResolvableApiException) e;
                    resolvable.startResolutionForResult(this, LOCATION_SETTINGS_REQUEST_CODE);
                } catch (IntentSender.SendIntentException sendEx) {
                    // Impossible d'afficher le dialogue.
                }
            }
        });
    }


    private void addCheckBoxesToLayout(LinearLayout layout, String[] servicesArray) {
        for (String service : servicesArray) {
            CheckBox checkBox = new CheckBox(this);
            checkBox.setText(service);
            layout.addView(checkBox);
        }
    }

    // Ajoutez cette méthode à votre classe MainActivity
    private boolean isValidDoubleString(double value) {
        // Ajoutez ici votre logique de vérification pour déterminer si le double est valide
        // Par exemple, vous pouvez vérifier si la valeur est différente de zéro ou d'une autre condition selon vos besoins.
        return value != 0.0;
    }

    private boolean isLocationServiceEnabled() {
        return locationManager != null && locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
    }


    private void handleLocationUpdate(Location location) {
        Log.d("TESTT", "CA marche !!");
        // Utiliser la nouvelle localisation pour vos besoins
        Log.d("TESTTTT", "Longitude: " + location.getLongitude());

        // Reste du code pour mettre à jour les distances, etc.
        currentLatitude = location.getLatitude();
        currentLongitude = location.getLongitude();
        String updatedUrl = BASE_API_URL
                .replace("<longitude>", isValidDoubleString(currentLongitude) ? String.valueOf(currentLongitude) : "0.0")
                .replace("<latitude>", isValidDoubleString(currentLatitude) ? String.valueOf(currentLatitude) : "0.0")
                .replace("<rayon>", rayon);

        // Utilisez l'URL mise à jour pour la tâche de récupération des données
        new FetchDataTask().execute(updatedUrl);

        // Request location updates if not on Android 10 or later
        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.Q) {
            // Vous pouvez également demander des mises à jour de localisation ici si nécessaire.
        }
    }

    private void showSettingsDialog() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Préférences");

        // Inflater la vue du dialogue personnalisé
        View dialogView = LayoutInflater.from(this).inflate(R.layout.custom_dialog_layout, null);
        builder.setView(dialogView);

        // Obtenir une référence au TextView pour afficher les informations sur le rayon
        TextView radiusInfoTextView = dialogView.findViewById(R.id.radiusInfoTextView);

        // Obtenir une référence au curseur
        SeekBar radiusSeekBar = dialogView.findViewById(R.id.radiusSeekBar);

        int rayonValue = Integer.parseInt(rayon);
        radiusSeekBar.setProgress(rayonValue);

        // Mettre à jour le texte avec les informations initiales
        updateRadiusInfo(radiusInfoTextView, radiusSeekBar.getProgress());

        // Utilisez dialogView pour obtenir la référence au layout des services
        LinearLayout servicesLayout = dialogView.findViewById(R.id.servicesLayout);

        Spinner fuelTypeSpinner = dialogView.findViewById(R.id.fuelTypeSpinner);
        Log.d("Location", "LES CARBURANTS aaa" + preference_carburant);
        if (!preference_carburant.equals("")) {
            Log.d("Location", "LES CARBURANTS");

            // Trouver l'index de la valeur dans la liste des éléments du Spinner
            ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.fuel_types, android.R.layout.simple_spinner_item);
            int index = adapter.getPosition(preference_carburant);

            // Définir la sélection dans le Spinner
            fuelTypeSpinner.setSelection(index);
        }

        // Définissez la liste des services

        // Créer une liste dynamique pour stocker les services sélectionnés


        // Ajoutez dynamiquement les cases à cocher pour tous les services
        if (servicesLayout != null) {
            for (int i = 0; i < allServicesArray.length; i++) {
                CheckBox checkBox = new CheckBox(this);
                checkBox.setText(allServicesArray[i]);
                servicesLayout.addView(checkBox);

                if (selectedServicesList.contains(allServicesArray[i])) {
                    checkBox.setChecked(true);
                }

                // Ajoutez un écouteur pour chaque case à cocher
                int finalI = i; // Besoin d'une variable finale pour être utilisée dans l'écouteur
                checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        // Mettez à jour la liste des services sélectionnés
                        if (isChecked) {
                            // Ajouter le service à la liste si coché
                            selectedServicesList.add(allServicesArray[finalI]);
                        } else {
                            // Retirer le service de la liste s'il est décoché
                            selectedServicesList.remove(allServicesArray[finalI]);
                        }
                    }
                });
            }
        } else {
            Log.e("MainActivity", "Le layout servicesLayout est null");
        }

        // Ajouter un écouteur de changement pour mettre à jour dynamiquement le texte
        radiusSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                // Mettre à jour le texte avec la nouvelle valeur du curseur
                updateRadiusInfo(radiusInfoTextView, progress);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                // Ne rien faire ici
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                // Ne rien faire ici
            }
        });


        // Vérifier si la chaîne "preference_carburant" n'est pas vide


        // Ajouter les boutons "Enregistrer" et "Annuler"
        builder.setPositiveButton("Enregistrer", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Traitement lorsque le bouton Enregistrer est cliqué
                String rayon2 = rayon;
                String preference_carburant2 = preference_carburant;

                rayon = String.valueOf(radiusSeekBar.getProgress());
                preference_carburant = fuelTypeSpinner.getSelectedItem().toString();
                refreshData();

                try {
                    writeJsonFile(jsonFile);
                } catch (IOException e) {
                    throw new RuntimeException(e);
                } catch (JSONException e) {
                    throw new RuntimeException(e);
                }


            }
        });

        builder.setNegativeButton("Annuler", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Traitement lorsque le bouton Annuler est cliqué
            }
        });

        builder.create().show();
        builder.setCancelable(false);
    }

    private void showSettingsDialog2() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Préférences");

        // Inflater la vue du dialogue personnalisé
        View dialogView = LayoutInflater.from(this).inflate(R.layout.custom_dialog_layout, null);
        builder.setView(dialogView);

        // Obtenir une référence au TextView pour afficher les informations sur le rayon
        TextView radiusInfoTextView = dialogView.findViewById(R.id.radiusInfoTextView);

        // Obtenir une référence au curseur
        SeekBar radiusSeekBar = dialogView.findViewById(R.id.radiusSeekBar);

        int rayonValue = Integer.parseInt(rayon);
        radiusSeekBar.setProgress(rayonValue);

        // Mettre à jour le texte avec les informations initiales
        updateRadiusInfo(radiusInfoTextView, radiusSeekBar.getProgress());

        // Utilisez dialogView pour obtenir la référence au layout des services
        LinearLayout servicesLayout = dialogView.findViewById(R.id.servicesLayout);


        Spinner fuelTypeSpinner = dialogView.findViewById(R.id.fuelTypeSpinner);
        Log.d("Location", "LES CARBURANTS aaa" + preference_carburant);
        if (!preference_carburant.equals("")) {
            Log.d("Location", "LES CARBURANTS");

            // Trouver l'index de la valeur dans la liste des éléments du Spinner
            ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(this, R.array.fuel_types, android.R.layout.simple_spinner_item);
            int index = adapter.getPosition(preference_carburant);

            // Définir la sélection dans le Spinner
            fuelTypeSpinner.setSelection(index);
        }

        // Définissez la liste des services

        // Créer une liste dynamique pour stocker les services sélectionnés


        // Ajoutez dynamiquement les cases à cocher pour tous les services
        if (servicesLayout != null) {
            for (int i = 0; i < allServicesArray.length; i++) {
                CheckBox checkBox = new CheckBox(this);
                checkBox.setText(allServicesArray[i]);
                servicesLayout.addView(checkBox);

                if (selectedServicesList.contains(allServicesArray[i])) {
                    checkBox.setChecked(true);
                }

                // Ajoutez un écouteur pour chaque case à cocher
                int finalI = i; // Besoin d'une variable finale pour être utilisée dans l'écouteur
                checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        // Mettez à jour la liste des services sélectionnés
                        if (isChecked) {
                            // Ajouter le service à la liste si coché
                            selectedServicesList.add(allServicesArray[finalI]);
                        } else {
                            // Retirer le service de la liste s'il est décoché
                            selectedServicesList.remove(allServicesArray[finalI]);
                        }
                    }
                });
            }
        } else {
            Log.e("MainActivity", "Le layout servicesLayout est null");
        }

        // Ajouter un écouteur de changement pour mettre à jour dynamiquement le texte
        radiusSeekBar.setOnSeekBarChangeListener(new SeekBar.OnSeekBarChangeListener() {
            @Override
            public void onProgressChanged(SeekBar seekBar, int progress, boolean fromUser) {
                // Mettre à jour le texte avec la nouvelle valeur du curseur
                updateRadiusInfo(radiusInfoTextView, progress);
            }

            @Override
            public void onStartTrackingTouch(SeekBar seekBar) {
                // Ne rien faire ici
            }

            @Override
            public void onStopTrackingTouch(SeekBar seekBar) {
                // Ne rien faire ici
            }
        });


        // Vérifier si la chaîne "preference_carburant" n'est pas vide


        // Ajouter les boutons "Enregistrer" et "Annuler"
        builder.setPositiveButton("Enregistrer", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Traitement lorsque le bouton Enregistrer est cliqué
                String rayon2 = rayon;
                String preference_carburant2 = preference_carburant;
                //Spinner fuelTypeSpinner = dialogView.findViewById(R.id.fuelTypeSpinner);
                if (selectedTabPosition == 0 && !rayon2.equals(String.valueOf(radiusSeekBar.getProgress()))) {
                    rayon = String.valueOf(radiusSeekBar.getProgress());
                    // Obtenez la valeur sélectionnée dans le Spinner
                    preference_carburant = fuelTypeSpinner.getSelectedItem().toString();
                    Log.d("Location", "PAPPAPAP");
                    refreshData();
                } else preference_carburant = fuelTypeSpinner.getSelectedItem().toString();

                if (selectedTabPosition == 1 && !preference_carburant2.equals(fuelTypeSpinner.getSelectedItem().toString())) {
                    rayon = String.valueOf(radiusSeekBar.getProgress());
                    preference_carburant = fuelTypeSpinner.getSelectedItem().toString();
                    refreshData();
                } else rayon = String.valueOf(radiusSeekBar.getProgress());

                if (selectedTabPosition == 2) {
                    rayon = String.valueOf(radiusSeekBar.getProgress());
                    preference_carburant = fuelTypeSpinner.getSelectedItem().toString();
                    refreshData();
                }

                try {
                    writeJsonFile(jsonFile);
                } catch (IOException e) {
                    throw new RuntimeException(e);
                } catch (JSONException e) {
                    throw new RuntimeException(e);
                }

                // Parcourez la liste des services sélectionnés et faites quelque chose avec chaque service


                //refreshData();
            }
        });


        builder.create().show();
        builder.setCancelable(false);
    }

    private void showSettingsCarburants() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Préférence Carburant");

        // Inflater la vue du dialogue personnalisé
        View dialogView = LayoutInflater.from(this).inflate(R.layout.custom_dialog_carburants, null);
        builder.setView(dialogView);

        AlertDialog alertDialog = builder.create();

        // Obtenez les dimensions de l'écran
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);

        int width = displayMetrics.widthPixels; // Largeur de l'écran
        int height = displayMetrics.heightPixels; // Hauteur de l'écran

        // Définir la largeur et la hauteur du dialogue en pourcentage de l'écran
        int dialogWidth = (int) (width * 0.9); // Par exemple, 90% de la largeur de l'écran
        int dialogHeight = (int) (height * 0.9); // Par exemple, 90% de la hauteur de l'écran

        // Appliquer les dimensions au dialogue
        alertDialog.getWindow().setLayout(dialogWidth, dialogHeight);

        // Empêcher la fermeture de la boîte de dialogue lors d'un clic en dehors de celle-ci
        alertDialog.setCancelable(false);

        // Ajouter les boutons "Enregistrer" et "Annuler" à l'instance de AlertDialog
        alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "Enregistrer", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Traitement lorsque le bouton Enregistrer est cliqué
                //refreshData();
                Spinner fuelTypeSpinner = dialogView.findViewById(R.id.fuelTypeSpinner);
                // Obtenez la valeur sélectionnée dans le Spinner
                preference_carburant = fuelTypeSpinner.getSelectedItem().toString();
                showLoading();
                try {
                    writeJsonFile(jsonFile);
                } catch (IOException e) {
                    throw new RuntimeException(e);
                } catch (JSONException e) {
                    throw new RuntimeException(e);
                }
                updatePricesAndSort(preference_carburant, selectedServicesList);

            }
        });

        alertDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Annuler", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Traitement lorsque le bouton Annuler est cliqué
                TabLayout.Tab firstTab = tabLayout.getTabAt(0);
                if (firstTab != null) {
                    firstTab.select();
                }
            }
        });

        alertDialog.show();
    }


    private void showSettingsServices() {
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle("Préférence Services");

        // Inflater la vue du dialogue personnalisé
        View dialogView = LayoutInflater.from(this).inflate(R.layout.custom_dialog_services, null);
        builder.setView(dialogView);

        AlertDialog alertDialog = builder.create();

        // Obtenez les dimensions de l'écran
        DisplayMetrics displayMetrics = new DisplayMetrics();
        getWindowManager().getDefaultDisplay().getMetrics(displayMetrics);

        int width = displayMetrics.widthPixels; // Largeur de l'écran
        int height = displayMetrics.heightPixels; // Hauteur de l'écran

        // Définir la largeur et la hauteur du dialogue en pourcentage de l'écran
        int dialogWidth = (int) (width * 0.9); // Par exemple, 90% de la largeur de l'écran
        int dialogHeight = (int) (height * 0.9); // Par exemple, 90% de la hauteur de l'écran

        // Appliquer les dimensions au dialogue
        alertDialog.getWindow().setLayout(dialogWidth, dialogHeight);

        // Empêcher la fermeture de la boîte de dialogue lors d'un clic en dehors de celle-ci
        alertDialog.setCancelable(false);

        // Utilisez dialogView pour obtenir la référence au layout des services
        LinearLayout servicesLayout = dialogView.findViewById(R.id.servicesLayout);

        // Définissez la liste des services

        // Créer une liste dynamique pour stocker les services sélectionnés


        // Ajoutez dynamiquement les cases à cocher pour tous les services
        if (servicesLayout != null) {
            for (int i = 0; i < allServicesArray.length; i++) {
                CheckBox checkBox = new CheckBox(this);
                checkBox.setText(allServicesArray[i]);
                servicesLayout.addView(checkBox);

                // Ajoutez un écouteur pour chaque case à cocher
                int finalI = i; // Besoin d'une variable finale pour être utilisée dans l'écouteur
                checkBox.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                        // Mettez à jour la liste des services sélectionnés
                        if (isChecked) {
                            // Ajouter le service à la liste si coché
                            selectedServicesList.add(allServicesArray[finalI]);
                        } else {
                            // Retirer le service de la liste s'il est décoché
                            selectedServicesList.remove(allServicesArray[finalI]);
                        }
                    }
                });
            }
        } else {
            Log.e("MainActivity", "Le layout servicesLayout est null");
        }

        // Ajouter les boutons "Enregistrer" et "Annuler" à l'instance de AlertDialog
        alertDialog.setButton(DialogInterface.BUTTON_POSITIVE, "Enregistrer", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                try {
                    writeJsonFile(jsonFile);
                } catch (IOException e) {
                    throw new RuntimeException(e);
                } catch (JSONException e) {
                    throw new RuntimeException(e);
                }
                refreshData();

            }
        });

        alertDialog.setButton(DialogInterface.BUTTON_NEGATIVE, "Annuler", new DialogInterface.OnClickListener() {
            @Override
            public void onClick(DialogInterface dialog, int which) {
                // Traitement lorsque le bouton Annuler est cliqué
                TabLayout.Tab firstTab = tabLayout.getTabAt(0);
                if (firstTab != null) {
                    firstTab.select();
                }
            }
        });

        alertDialog.show();
    }


    // Méthode pour mettre à jour le texte des informations sur le rayon
    private void updateRadiusInfo(TextView textView, int progress) {
        String info = "Rayon: " + progress + " km (1-50 km)";
        textView.setText(info);
    }


    private void refreshData() {
        // Update the URL with the new location data
        //textView.setText("");
        buttonContainer.removeAllViews();
        stationList.clear();


        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
            FusedLocationProviderClient mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
            mFusedLocationClient.getLastLocation()
                    .addOnSuccessListener(this, new OnSuccessListener<Location>() {
                        @Override
                        public void onSuccess(Location location) {
                            // La localisation GPS peut être nulle si le GPS est désactivé
                            if (location != null) {
                                Log.d("Location", "aaaaaaaaa");
                                double latitude = location.getLatitude();
                                double longitude = location.getLongitude();
                                Log.d("Location", "Latitude: " + latitude + ", Longitude: " + longitude);

                                currentLatitude = latitude;
                                currentLongitude = longitude;

                                String updatedUrl = BASE_API_URL
                                        .replace("<longitude>", String.valueOf(currentLongitude))
                                        .replace("<latitude>", String.valueOf(currentLatitude))
                                        .replace("<rayon>", rayon);
                                Log.d("Location", "Latitude: " + updatedUrl);


                                // Utilisez l'URL mise à jour pour la tâche de récupération des données

                                new FetchDataTask().execute(updatedUrl);
                            } else
                                Log.d("Location", "Erreur lors de la tentative d'obtention de la dernière localisation GPS");

                        }
                    })
                    .addOnFailureListener(this, new OnFailureListener() {
                        @Override
                        public void onFailure(@NonNull Exception e) {
                            Log.d("MapDemoActivity", "Erreur lors de la tentative d'obtention de la dernière localisation GPS");
                            e.printStackTrace();
                        }
                    });
        }





     /*   updatedUrl = BASE_API_URL
                .replace("<longitude>", String.valueOf(currentLongitude))
                .replace("<latitude>", String.valueOf(currentLatitude))
                .replace("<rayon>", rayon);

        // Execute the AsyncTask to fetch data
        Log.d("FetchDataTask", "Site'"+updatedUrl);
        new FetchDataTask().execute(updatedUrl);*/
    }


/*
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        Log.d("TESTTTT", "LongitudeZZaaaaaaaaZ: ");

        if (requestCode == LOCATION_PERMISSION_REQUEST_CODE) {
            locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // La permission ACCESS_FINE_LOCATION a été accordée
                if (checkSelfPermission(ACCESS_FINE_LOCATION) == PackageManager.PERMISSION_GRANTED) {
                    // Vérifiez également la permission ACCESS_BACKGROUND_LOCATION
                    if (checkSelfPermission(Manifest.permission.ACCESS_BACKGROUND_LOCATION) == PackageManager.PERMISSION_GRANTED || Build.VERSION.SDK_INT < Build.VERSION_CODES.Q) {
                        // Get the last known location (may be null)
                        Location lastKnownLocation = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
                        Log.d("TESTTTT", "LongitudeZZZZZZ: ");

                        if (lastKnownLocation != null) {
                            // Call the logic to update distances immediately with the last known location
                            Log.d("TESTTTT", "LongitudeZZZ: " + lastKnownLocation.getLongitude());
                            String updatedUrl = BASE_API_URL
                                    .replace("<longitude>", isValidDoubleString(lastKnownLocation.getLongitude()) ? String.valueOf(lastKnownLocation.getLongitude()) : "0.0")
                                    .replace("<latitude>", isValidDoubleString(lastKnownLocation.getLatitude()) ? String.valueOf(lastKnownLocation.getLatitude()) : "0.0")
                                    .replace("<rayon>", rayon);

                            currentLatitude = lastKnownLocation.getLatitude();
                            currentLongitude = lastKnownLocation.getLongitude();
                            // Utilisez l'URL mise à jour pour la tâche de récupération des données
                            new FetchDataTask().execute(updatedUrl);
                        } else {
                            Log.d("CA marche pas", "CA marche pas22");
                        }
                    } else {
                        // L'utilisateur n'a pas accordé la permission ACCESS_BACKGROUND_LOCATION
                        // Vous pouvez afficher un message ou prendre d'autres mesures
                        Log.d("CA marche pas", "Permission ACCESS_BACKGROUND_LOCATION non accordée");
                    }
                }
            } else {
                // L'utilisateur a refusé l'autorisation ACCESS_FINE_LOCATION, vous pouvez afficher un message ou prendre d'autres mesures
                // Expliquez à l'utilisateur pourquoi la localisation est nécessaire et invitez-le à accorder l'autorisation
                Log.d("CA marche pas", "Permission ACCESS_FINE_LOCATION non accordée");
            }
        }
    }*/


    /*
    private void updateDistances(double currentLatitude, double currentLongitude) {
        if (result != null) {
            try {
                JSONObject responseJson = new JSONObject(result);
                Log.d("OOOOOOOOOOOOOOOO", "Address: ");

                if (responseJson.has("records")) {
                    JSONArray records = responseJson.getJSONArray("records");



                    for (int i = 0; i < records.length(); i++) {
                        JSONObject record = records.getJSONObject(i);

                        if (record.has("fields")) {
                            JSONObject fields = record.getJSONObject("fields");

                            if (fields.has("adresse") && fields.has("latitude") && fields.has("longitude")) {
                                String address = fields.getString("adresse");
                                double stationLatitude = fields.getDouble("latitude");
                                double stationLongitude = fields.getDouble("longitude");

                                // Calculez la distance entre la station et la localisation actuelle
                                float[] results = new float[1];
                                Location.distanceBetween(currentLatitude, currentLongitude, stationLatitude, stationLongitude, results);
                                double distance = results[0] / 1000.0;  // Convertissez en kilomètres

                                // Créez un objet Station et ajoutez-le à la liste
                                Station station = new Station(address, distance);

                                station.setAddress(address);
                                station.setDistance(distance);
                                stationList.add(station);

                                Log.d("FetchDataTask", "Address: " + address + " - Distance: " + distance + " km");
                            }
                        } else {
                            Log.e("FetchDataTask", "Key 'fields' not found in the record.");
                        }
                    }

                    // Triez la liste des stations par distance
                    Collections.sort(stationList, new Comparator<Station>() {
                        @Override
                        public int compare(Station station1, Station station2) {
                            return Double.compare(station1.getDistance(), station2.getDistance());
                        }
                    });

                    // Affichez la liste triée dans le TextView
                    textView.setText("");  // Effacez le contenu précédent
                    for (Station station : stationList) {
                        textView.append(station.getAddress() + " - Distance: " + station.getDistance() + " km\n");
                    }
                } else {
                    Log.e("FetchDataTask", "Key 'records' not found in the JSON response.");
                }

            } catch (JSONException e) {
                Log.e("FetchDataTask", "Error parsing JSON: " + e.getMessage());
                e.printStackTrace();
            }
        }
    }*/
    private void updateTextViewContent(int selectedTabPosition) {
        switch (selectedTabPosition) {
            case 0:
                // Onglet "Localisation Paris"
                //textView.setText("Contenu pour Localisation Paris");
                // showLoading();
                // textView.setText("");
                buttonContainer.removeAllViews();
                //stationList.clear();
                //refreshData();
                showLoading();
                updateDistancesAndSort(preference_carburant, selectedServicesList);
                // if (selectedServicesList.isEmpty()||preference_carburant.isEmpty()) showSettingsDialog();
                break;
            case 1:
                //  textView.setText("Veuillez sélectionner une préférence de carburant");
                // Onglet "Moins Chère"
                //textView.setText("Moins chères");
                if (preference_carburant.isEmpty()) {
                    buttonContainer.removeAllViews();
                    //  showSettingsDialog();
               /* TabLayout.Tab firstTab = tabLayout.getTabAt(0);
                if (firstTab != null) {
                    firstTab.select();
                }*/
                    showSettingsCarburants();
                } else {
                    // textView.setText("");
                    showLoading();

                    updatePricesAndSort(preference_carburant, selectedServicesList);
                }
                break;
      /*  case 2:
            // Onglet "Moins Chère"
            // textView.setText("Préférences services");
           // textView.setText("La liste des services est vide.");
            showLoading();

            if (!selectedServicesList.isEmpty()) {
                buttonContainer.removeAllViews();
                //textView.setText("");
                updateServicesAndSort(selectedServicesList);
            } else {
                // Faites quelque chose si la liste est vide, par exemple afficher un message
               /* TabLayout.Tab firstTab = tabLayout.getTabAt(0);
                if (firstTab != null) {
                    firstTab.select();
                }*/
            //showSettingsDialog();
            /*    buttonContainer.removeAllViews();
                hideLoading(); // Assurez-vous de masquer le chargement si la liste est vide
                showSettingsServices();
            }
            break;*/


        }
    }

    private void showLoading() {
        progressBar.setVisibility(View.VISIBLE);
    }

    private void hideLoading() {
        progressBar.setVisibility(View.GONE);
    }

    private double calculateDistance(double startLatitude, double startLongitude, double endLatitude, double endLongitude) {
        Log.d("TESTTTT", "Longitude: " + startLongitude);
        Log.d("TESTTTT", "EndLongitude: " + endLongitude);
        // Haversine formula to calculate distance between two points on the earth
        double dLat = Math.toRadians(endLatitude - startLatitude);
        double dLon = Math.toRadians(endLongitude - startLongitude);

        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
                Math.cos(Math.toRadians(startLatitude)) * Math.cos(Math.toRadians(endLatitude)) *
                        Math.sin(dLon / 2) * Math.sin(dLon / 2);

        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));

        // Radius of the earth in kilometers
        double radius = 6371.0;
        Log.d("TESTTTT", "Longitude: aaaaaaaaaaaaaaaaazzzzzzzzzzzaaaa" + radius * c);

        return radius * c;

    }

    /* private void updatePricesAndSort(String carburant) {
         // Create a new list to store stations selling E10, sorted by E10 price
         List<Station> e10StationList = new ArrayList<>();

         for (Station station : stationList) {
             // Check if the station sells E10
             if (station.getE10_prix() > 0) {
                 // Create a new Station object with E10 price
                 e10StationList.add(station);
             }
         }

         // Sort the E10 station list by E10 price (from least to most expensive)
         Collections.sort(e10StationList, new Comparator<Station>() {
             @Override
             public int compare(Station station1, Station station2) {
                 return Double.compare(station1.getE10_prix(), station2.getE10_prix());
             }
         });

         // Display the sorted E10 station list in the TextView or perform other operations
         textView.setText("");  // Clear previous content
         hideLoading();
         for (Station station : e10StationList) {
             textView.append(station.getAddress() + " - E10 Price: " + station.getE10_prix() + "\n");
         }
     }*/
    private void updateServicesAndSort(List<String> selectedServicesArray2) {
        // Créer une nouvelle liste pour stocker les stations correspondant aux services sélectionnés
        List<Station> filteredStationList = new ArrayList<>();

        for (Station station : stationList) {
            // Vérifier si la station a des services correspondant aux services sélectionnés
            if (containsAllServices(station, selectedServicesArray2)) {
                // La station a tous les services sélectionnés

                // Calculer la distance ici
                double distance = calculateDistance(currentLatitude, currentLongitude, station.getLatitude(), station.getLongitude());

                // Mettre à jour la distance dans l'objet Station
                station.setDistance(distance);

                filteredStationList.add(station);
            }
        }

        // Trier la liste des stations filtrées par la distance
        Collections.sort(filteredStationList, new Comparator<Station>() {
            @Override
            public int compare(Station station1, Station station2) {
                // Comparaison basée sur la distance
                return Double.compare(station1.getDistance(), station2.getDistance());
            }
        });

        // Afficher la liste triée dans le TextView ou effectuer d'autres opérations

        buttonContainer.removeAllViews();
        hideLoading();


        int lightGrayColor = Color.parseColor("#605c5b");
        int countStations = 0;
        for (int i = 0; i < filteredStationList.size(); i++) {
            Station station = filteredStationList.get(i);

// Formatage de la distance avec trois chiffres après la virgule
            DecimalFormat decimalFormat = new DecimalFormat("0.000");
            String formattedDistance = decimalFormat.format(station.getDistance());

            // Remplacement de la virgule par un point
            formattedDistance = formattedDistance.replace(",", ".");

            // Create a new Button for each station
            Button stationButton = new Button(this);
            stationButton.setText(station.getAddress() + " - Distance: " + formattedDistance);
            stationButton.setTextColor(Color.WHITE);

            MaterialShapeDrawable shapeDrawable = new MaterialShapeDrawable();
            shapeDrawable.setFillColor(ColorStateList.valueOf(lightGrayColor)); // Use ColorStateList
            shapeDrawable.setShapeAppearanceModel(
                    ShapeAppearanceModel.builder()
                            .setAllCorners(CornerFamily.ROUNDED, 25.0f)
                            .build()
            );

            ViewCompat.setBackground(stationButton, shapeDrawable);


            // Set layout parameters for smaller buttons
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            );

            // Adjust the width and height as needed
            params.setMargins(16, 8, 16, 8); // Add margins for spacing

            stationButton.setLayoutParams(params);

            stationButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // Handle button click for this station
                    // You can perform any actions or open a new activity/fragment here
                    Intent intent = new Intent(MainActivity.this, StationActivity.class);
                    //startActivity(intent);
                    intent.putExtra("selectedStation", station);

                    // Start StationActivity
                    startActivity(intent);
                }
            });
            countStations++;
            // Add the button to the LinearLayout
            buttonContainer.addView(stationButton);
// ...
        }
        if (countStations == 0) {

            Button stationButton = new Button(this);
            stationButton.setText("Pas de Stations Pour Votre Demande de Services");
            stationButton.setTextColor(Color.WHITE);

            MaterialShapeDrawable shapeDrawable = new MaterialShapeDrawable();
            shapeDrawable.setFillColor(ColorStateList.valueOf(lightGrayColor)); // Use ColorStateList
            shapeDrawable.setShapeAppearanceModel(
                    ShapeAppearanceModel.builder()
                            .setAllCorners(CornerFamily.ROUNDED, 25.0f)
                            .build()
            );

            ViewCompat.setBackground(stationButton, shapeDrawable);


            // Set layout parameters for smaller buttons
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            );

            // Adjust the width and height as needed
            params.setMargins(16, 8, 16, 8); // Add margins for spacing

            stationButton.setLayoutParams(params);

            buttonContainer.addView(stationButton);

        }
    }


    private boolean containsAllServices(Station station, List<String> selectedServicesArray2) {
        // Utilisez la méthode appropriée pour obtenir les services de la station (par exemple, getServices())
        String stationServicesJson = station.getServices();
        Log.d("TESTTTT", "aaaaaaaaaaaaa" + stationServicesJson);

        try {
            // Convertir la chaîne JSON en un objet JSON
            JSONObject stationServicesObj = new JSONObject(stationServicesJson);

            // Obtenir le tableau JSON associé à la clé "service"
            JSONArray stationServicesArray = stationServicesObj.getJSONArray("service");

            // Convertir le tableau JSON en un tableau de chaînes
            String[] stationServicesArrayString = new String[stationServicesArray.length()];
            for (int i = 0; i < stationServicesArray.length(); i++) {
                stationServicesArrayString[i] = stationServicesArray.getString(i);
            }

            // Log pour vérifier le contenu du tableau


            // Vérifier si tous les services sélectionnés sont présents dans la station
            for (String selectedService : selectedServicesArray2) {
                if (!stationServicesJson.contains(selectedService)) {
                    return false;
                }
            }

            return true;
        } catch (JSONException e) {
            e.printStackTrace();
            return false;
        }
    }


    private void updatePricesAndSort(String carburant, List<String> selectedServicesArray2) {
        // Create a new list to store stations selling the specified carburant, sorted by carburant price
        List<Station> carburantStationList = new ArrayList<>();

        for (Station station : stationList) {
            // Check if the station sells the specified carburant
            double carburantPrice = 0.0;

            // Determine which carburant price to use based on the specified carburant type
            switch (carburant) {
                case "SP95":
                    carburantPrice = station.getSp95_prix();
                    break;
                case "SP98":
                    carburantPrice = station.getSp98_prix();
                    break;
                case "E10":
                    carburantPrice = station.getE10_prix();
                    break;
                case "E85":
                    carburantPrice = station.getE85_prix();
                    break;
                case "GPLc":
                    carburantPrice = station.getGplc_prix();
                    break;
                case "Gazole":
                    carburantPrice = station.getGazole_prix();
                    break;
            }


            // Vérifier si la station a des services correspondant aux services sélectionnés
            if (containsAllServices(station, selectedServicesArray2) && carburantPrice > 0) {

                carburantStationList.add(station);
            }
        }

        // Sort the carburant station list by carburant price (from least to most expensive)
        Collections.sort(carburantStationList, new Comparator<Station>() {
            @Override
            public int compare(Station station1, Station station2) {
                double price1, price2;

                // Determine which carburant price to use based on the specified carburant type
                switch (carburant) {
                    case "SP95":
                        price1 = station1.getSp95_prix();
                        price2 = station2.getSp95_prix();
                        break;
                    case "SP98":
                        price1 = station1.getSp98_prix();
                        price2 = station2.getSp98_prix();
                        break;
                    case "E10":
                        price1 = station1.getE10_prix();
                        price2 = station2.getE10_prix();
                        break;
                    case "E85":
                        price1 = station1.getE85_prix();
                        price2 = station2.getE85_prix();
                        break;
                    case "GPLc":
                        price1 = station1.getGplc_prix();
                        price2 = station2.getGplc_prix();
                        break;
                    case "Gazole":
                        price1 = station1.getGazole_prix();
                        price2 = station2.getGazole_prix();
                        break;
                    default:
                        price1 = 0.0;
                        price2 = 0.0;
                        break;
                }

                return Double.compare(price1, price2);
            }
        });

        // Display the sorted carburant station list in the TextView or perform other operations
        // textView.setText("");  // Clear previous content
        buttonContainer.removeAllViews();
        hideLoading();


        int lightGrayColor = Color.parseColor("#605c5b");
        int orangeColor = Color.parseColor("#fbcc92");
        int countStations = 0;
        int maxButtonWidth = 0;
        List<Button> trajetButtons = new ArrayList<>();

// Variable pour stocker la hauteur maximale
        int maxButtonHeight = 0;

        for (int i = 0; i < carburantStationList.size(); i++) {
            Station station = carburantStationList.get(i);

            LinearLayout horizontalLayout = new LinearLayout(this);
            horizontalLayout.setLayoutParams(new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.WRAP_CONTENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            ));

            horizontalLayout.setOrientation(LinearLayout.HORIZONTAL);

            // Create a new Button for each station
            Button stationButton = new Button(this);
            stationButton.setText(station.getAddress() + " - " + carburant + " Price: " + getCarburantPrice(station, carburant) + '\n');
            stationButton.setTextColor(Color.WHITE);
            float textSizeInSp = 12; // ajuste cette valeur selon tes préférences
            // float textSizeInSp = 12.5F;
            stationButton.setTextSize(TypedValue.COMPLEX_UNIT_SP, textSizeInSp);

            MaterialShapeDrawable shapeDrawable = new MaterialShapeDrawable();
            shapeDrawable.setFillColor(ColorStateList.valueOf(lightGrayColor));
            shapeDrawable.setShapeAppearanceModel(
                    ShapeAppearanceModel.builder()
                            .setAllCorners(CornerFamily.ROUNDED, 25.0f)
                            .build()
            );

            ViewCompat.setBackground(stationButton, shapeDrawable);


            // Measure le bouton pour obtenir sa largeur réelle

            // Set layout parameters for larger buttons
            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    0,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            );

            params.weight = 1; // Utilise un poids plus grand pour agrandir le bouton

            stationButton.setLayoutParams(params);


            // Add onClickListener for station details
            stationButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // Handle button click for this station
                    Intent intent = new Intent(MainActivity.this, StationActivity.class);
                    intent.putExtra("selectedStation", station);
                    // Start StationActivity
                    startActivity(intent);
                }
            });

            stationButton.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
            int buttonWidth = stationButton.getMeasuredWidth();

            // Met à jour la largeur maximale si nécessaire
            if (buttonWidth > maxButtonWidth) {
                maxButtonWidth = buttonWidth;
            }

            // Met à jour la hauteur maximale si nécessaire
            int buttonHeight = stationButton.getMeasuredHeight();
            if (buttonHeight > maxButtonHeight) {
                maxButtonHeight = buttonHeight;
            }


            // Create "Trajet" button
            Button trajetButton = new Button(this);
            trajetButton.setText("Trajet");
            trajetButton.setTextColor(Color.WHITE);

            MaterialShapeDrawable trajetShapeDrawable = new MaterialShapeDrawable();
            trajetShapeDrawable.setFillColor(ColorStateList.valueOf(orangeColor));
            trajetShapeDrawable.setShapeAppearanceModel(
                    ShapeAppearanceModel.builder()
                            .setAllCorners(CornerFamily.ROUNDED, 25.0f)
                            .build()
            );

            ViewCompat.setBackground(trajetButton, trajetShapeDrawable);

            LinearLayout.LayoutParams trajetParams = new LinearLayout.LayoutParams(
                    0,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            );
            trajetParams.weight = 1.5F; // Utilise layout_weight
            trajetParams.setMargins(0, 200, 0, 0); // Add margins for spacing

            // Add onClickListener for "Trajet" action
            trajetButton.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    // Ouvre Google Maps avec l'adresse spécifiée pour cette station
                    openGoogleMaps(station.getAddress());
                }
            });
            trajetButtons.add(trajetButton);

            // Add buttons to the horizontal layout
            horizontalLayout.addView(stationButton);
            horizontalLayout.addView(trajetButton);
            // Add the horizontal layout to the buttonContainer
            buttonContainer.addView(horizontalLayout);

            countStations++;


        }


        Log.d("Location", "la taille" + maxButtonWidth);
        for (int i = 0; i < buttonContainer.getChildCount(); i++) {
            View child = buttonContainer.getChildAt(i);
            if (child instanceof LinearLayout) {
                for (int j = 0; j < ((LinearLayout) child).getChildCount(); j++) {
                    View innerChild = ((LinearLayout) child).getChildAt(j);
                    if (innerChild instanceof Button) {
                        // Vérifie si le bouton est un bouton trajet
                        String buttonText = ((Button) innerChild).getText().toString().trim();
                        if (!buttonText.equalsIgnoreCase("TRAJET")) {
                               /* LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) innerChild.getLayoutParams();

                                params.height = maxButtonHeight;
                                innerChild.setLayoutParams(params);*/
                            LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) innerChild.getLayoutParams();
                            params.width = maxButtonWidth;
                            params.height = maxButtonHeight;
                            innerChild.setLayoutParams(params);
                        } else {
                            LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) innerChild.getLayoutParams();
                            Log.d("Location", String.valueOf(maxButtonWidth));
                            params.height = maxButtonHeight;
                            innerChild.setLayoutParams(params);
                        }
                    }
                }
            }
        }


        for (Button trajetButton : trajetButtons) {
            // Modifiez la hauteur du bouton selon vos besoins
            LinearLayout.LayoutParams trajetParams = (LinearLayout.LayoutParams) trajetButton.getLayoutParams();
            trajetParams.setMargins(0, 20, 0, 0); // Assurez-vous d'ajuster la valeur en conséquence

            trajetParams.height = maxButtonHeight;
            trajetButton.setLayoutParams(trajetParams);
        }


// If no stations are available, add a message button
        if (countStations == 0) {
            Button stationButton = new Button(this);
            stationButton.setText("Pas de Stations Selon Vos Critères");
            stationButton.setTextColor(Color.WHITE);

            MaterialShapeDrawable shapeDrawable = new MaterialShapeDrawable();
            shapeDrawable.setFillColor(ColorStateList.valueOf(lightGrayColor));
            shapeDrawable.setShapeAppearanceModel(
                    ShapeAppearanceModel.builder()
                            .setAllCorners(CornerFamily.ROUNDED, 25.0f)
                            .build()
            );

            ViewCompat.setBackground(stationButton, shapeDrawable);

            LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                    LinearLayout.LayoutParams.MATCH_PARENT,
                    LinearLayout.LayoutParams.WRAP_CONTENT
            );
            params.setMargins(16, 8, 16, 8); // Add margins for spacing

            stationButton.setLayoutParams(params);

            buttonContainer.addView(stationButton);
        }


    }

    // Helper method to get the price for the specified carburant from a Station
    private double getCarburantPrice(Station station, String carburant) {
        double price = 0.0;

        // Determine which carburant price to use based on the specified carburant type
        switch (carburant) {
            case "SP95":
                price = station.getSp95_prix();
                break;
            case "SP98":
                price = station.getSp98_prix();
                break;
            case "E10":
                price = station.getE10_prix();
                break;
            case "E85":
                price = station.getE85_prix();
                break;
            case "GPLc":
                price = station.getGplc_prix();
                break;
            case "Gazole":
                price = station.getGazole_prix();
                break;
        }

        return price;
    }


    private void updateDistancesAndSort(String carburant, List<String> selectedServicesArray2) {
        Log.d("TESTTTT", "Longitude: aaaaaaaaaaaaa" + currentLongitude + "et" + currentLatitude);

        // Check if the current location coordinates are available
        if (currentLatitude != 0 && currentLongitude != 0) {
            // Create a new list to store stations sorted by distance
            List<Station> sortedStationList = new ArrayList<>();

            for (Station station : stationList) {
                // Retrieve coordinates for each station
                double stationLatitude = station.getLatitude();
                double stationLongitude = station.getLongitude();

                if (stationLatitude != 0 && stationLongitude != 0) {
                    double distance = calculateDistance(currentLatitude, currentLongitude, stationLatitude, stationLongitude);


                    station.setDistance(distance);
                    if (containsAllServices(station, selectedServicesArray2)) {

                        sortedStationList.add(station);
                    }

                }
            }

            // Sort the sortedStationList by distance
            Collections.sort(sortedStationList, new Comparator<Station>() {
                @Override
                public int compare(Station station1, Station station2) {
                    return Double.compare(station1.getDistance(), station2.getDistance());
                }
            });

            List<Station> carburantStationList = new ArrayList<>();

            for (Station station : sortedStationList) {
                // Check if the station sells the specified carburant
                double carburantPrice = 0.0;

                // Determine which carburant price to use based on the specified carburant type
                switch (carburant) {
                    case "SP95":
                        carburantPrice = station.getSp95_prix();
                        break;
                    case "SP98":
                        carburantPrice = station.getSp98_prix();
                        break;
                    case "E10":
                        carburantPrice = station.getE10_prix();
                        break;
                    case "E85":
                        carburantPrice = station.getE85_prix();
                        break;
                    case "GPLc":
                        carburantPrice = station.getGplc_prix();
                        break;
                    case "Gazole":
                        carburantPrice = station.getGazole_prix();
                        break;
                }

                if (carburantPrice > 0) {
                    carburantStationList.add(station);
                }


                // carburantStationList.add(station);

            }


            // Display the sorted list in the TextView or perform other operations
            // textView.setText("");  // Clear previous content
            buttonContainer.removeAllViews();
            hideLoading();


            int lightGrayColor = Color.parseColor("#605c5b");
            int orangeColor = Color.parseColor("#fbcc92");
            int countStations = 0;
            int maxButtonWidth = 0;
            List<Button> trajetButtons = new ArrayList<>();

// Variable pour stocker la hauteur maximale
            int maxButtonHeight = 0;

            for (int i = 0; i < carburantStationList.size(); i++) {
                Station station = sortedStationList.get(i);

                // Formatage de la distance avec trois chiffres après la virgule
                DecimalFormat decimalFormat = new DecimalFormat("0.000");
                String formattedDistance = decimalFormat.format(station.getDistance());

                // Remplacement de la virgule par un point
                formattedDistance = formattedDistance.replace(",", ".");

                LinearLayout horizontalLayout = new LinearLayout(this);
                horizontalLayout.setLayoutParams(new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.WRAP_CONTENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT
                ));

                horizontalLayout.setOrientation(LinearLayout.HORIZONTAL);

                // Create a new Button for each station
                Button stationButton = new Button(this);
                stationButton.setText(station.getAddress() + " - " + formattedDistance + " km \n");
                stationButton.setTextColor(Color.WHITE);
                float textSizeInSp = 12; // ajuste cette valeur selon tes préférences
                // float textSizeInSp = 12.5F;
                stationButton.setTextSize(TypedValue.COMPLEX_UNIT_SP, textSizeInSp);

                MaterialShapeDrawable shapeDrawable = new MaterialShapeDrawable();
                shapeDrawable.setFillColor(ColorStateList.valueOf(lightGrayColor));
                shapeDrawable.setShapeAppearanceModel(
                        ShapeAppearanceModel.builder()
                                .setAllCorners(CornerFamily.ROUNDED, 25.0f)
                                .build()
                );

                ViewCompat.setBackground(stationButton, shapeDrawable);


                // Measure le bouton pour obtenir sa largeur réelle

                // Set layout parameters for larger buttons
                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                        0,
                        LinearLayout.LayoutParams.WRAP_CONTENT
                );

                params.weight = 1; // Utilise un poids plus grand pour agrandir le bouton

                stationButton.setLayoutParams(params);


                // Add onClickListener for station details
                stationButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        // Handle button click for this station
                        Intent intent = new Intent(MainActivity.this, StationActivity.class);
                        intent.putExtra("selectedStation", station);
                        // Start StationActivity
                        startActivity(intent);
                    }
                });

                stationButton.measure(View.MeasureSpec.UNSPECIFIED, View.MeasureSpec.UNSPECIFIED);
                int buttonWidth = stationButton.getMeasuredWidth();

                // Met à jour la largeur maximale si nécessaire
                if (buttonWidth > maxButtonWidth) {
                    maxButtonWidth = buttonWidth;
                }

                // Met à jour la hauteur maximale si nécessaire
                int buttonHeight = stationButton.getMeasuredHeight();
                if (buttonHeight > maxButtonHeight) {
                    maxButtonHeight = buttonHeight;
                }


                // Create "Trajet" button
                Button trajetButton = new Button(this);
                trajetButton.setText("Trajet");
                trajetButton.setTextColor(Color.WHITE);

                MaterialShapeDrawable trajetShapeDrawable = new MaterialShapeDrawable();
                trajetShapeDrawable.setFillColor(ColorStateList.valueOf(orangeColor));
                trajetShapeDrawable.setShapeAppearanceModel(
                        ShapeAppearanceModel.builder()
                                .setAllCorners(CornerFamily.ROUNDED, 25.0f)
                                .build()
                );

                ViewCompat.setBackground(trajetButton, trajetShapeDrawable);

                LinearLayout.LayoutParams trajetParams = new LinearLayout.LayoutParams(
                        0,
                        LinearLayout.LayoutParams.WRAP_CONTENT
                );
                trajetParams.weight = 1.5F; // Utilise layout_weight
                trajetParams.setMargins(0, 200, 0, 0); // Add margins for spacing

                // Add onClickListener for "Trajet" action
                trajetButton.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        // Ouvre Google Maps avec l'adresse spécifiée pour cette station
                        openGoogleMaps(station.getAddress());
                    }
                });
                trajetButtons.add(trajetButton);

                // Add buttons to the horizontal layout
                horizontalLayout.addView(stationButton);
                horizontalLayout.addView(trajetButton);
                // Add the horizontal layout to the buttonContainer
                buttonContainer.addView(horizontalLayout);

                countStations++;


            }


            Log.d("Location", "la taille" + maxButtonWidth);
            for (int i = 0; i < buttonContainer.getChildCount(); i++) {
                View child = buttonContainer.getChildAt(i);
                if (child instanceof LinearLayout) {
                    for (int j = 0; j < ((LinearLayout) child).getChildCount(); j++) {
                        View innerChild = ((LinearLayout) child).getChildAt(j);
                        if (innerChild instanceof Button) {
                            // Vérifie si le bouton est un bouton trajet
                            String buttonText = ((Button) innerChild).getText().toString().trim();
                            if (!buttonText.equalsIgnoreCase("TRAJET")) {
                               /* LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) innerChild.getLayoutParams();

                                params.height = maxButtonHeight;
                                innerChild.setLayoutParams(params);*/
                                LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) innerChild.getLayoutParams();
                                params.width = maxButtonWidth;
                                params.height = maxButtonHeight;
                                innerChild.setLayoutParams(params);
                            } else {
                                LinearLayout.LayoutParams params = (LinearLayout.LayoutParams) innerChild.getLayoutParams();
                                Log.d("Location", String.valueOf(maxButtonWidth));
                                params.height = maxButtonHeight;
                                innerChild.setLayoutParams(params);
                            }
                        }
                    }
                }
            }


            for (Button trajetButton : trajetButtons) {
                // Modifiez la hauteur du bouton selon vos besoins
                LinearLayout.LayoutParams trajetParams = (LinearLayout.LayoutParams) trajetButton.getLayoutParams();
                trajetParams.setMargins(0, 20, 0, 0); // Assurez-vous d'ajuster la valeur en conséquence

                trajetParams.height = maxButtonHeight;
                trajetButton.setLayoutParams(trajetParams);
            }


            if (countStations == 0) {
                Button stationButton = new Button(this);
                stationButton.setText("Pas de Stations Selon Vos Critères");
                stationButton.setTextColor(Color.WHITE);

                MaterialShapeDrawable shapeDrawable = new MaterialShapeDrawable();
                shapeDrawable.setFillColor(ColorStateList.valueOf(lightGrayColor));
                shapeDrawable.setShapeAppearanceModel(
                        ShapeAppearanceModel.builder()
                                .setAllCorners(CornerFamily.ROUNDED, 25.0f)
                                .build()
                );

                ViewCompat.setBackground(stationButton, shapeDrawable);

                LinearLayout.LayoutParams params = new LinearLayout.LayoutParams(
                        LinearLayout.LayoutParams.MATCH_PARENT,
                        LinearLayout.LayoutParams.WRAP_CONTENT
                );
                params.setMargins(16, 8, 16, 8); // Add margins for spacing

                stationButton.setLayoutParams(params);

                buttonContainer.addView(stationButton);
            }


        }
    }

    private void openGoogleMaps(String address) {
        // Créer une Uri avec l'adresse pour l'intention Google Maps
        Uri gmmIntentUri = Uri.parse("geo:0,0?q=" + Uri.encode(address));

        // Créer une intention pour ouvrir Google Maps avec l'adresse spécifiée
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
        mapIntent.setPackage("com.google.android.apps.maps");

        // Vérifier si l'application Google Maps est installée
        if (mapIntent.resolveActivity(getPackageManager()) != null) {
            startActivity(mapIntent);
        }
    }


    private class FetchDataTask extends AsyncTask<String, Void, String> {

        @Override
        protected String doInBackground(String... params) {
            try {
                URL url = new URL(params[0]);
                HttpURLConnection urlConnection = (HttpURLConnection) url.openConnection();

                try {
                    InputStream in = urlConnection.getInputStream();
                    BufferedReader reader = new BufferedReader(new InputStreamReader(in));
                    StringBuilder result = new StringBuilder();
                    String line;

                    while ((line = reader.readLine()) != null) {
                        result.append(line);
                    }

                    return result.toString();
                } finally {
                    urlConnection.disconnect();
                }
            } catch (IOException e) {
                e.printStackTrace();
                return null;
            }
        }

        @Override
        protected void onCancelled() {
            super.onCancelled();
            if (progressDialog != null && progressDialog.isShowing()) {
                progressDialog.dismiss();
                progressDialog = null; // Libérer la référence
            }
        }


        @Override
        protected void onPreExecute() {
            super.onPreExecute();
            progressDialog = new ProgressDialog(MainActivity.this);
            progressDialog.setMessage("Chargement en cours...");
            progressDialog.setCancelable(false);
            progressDialog.show();

            // Ajouter un délai avec ThreadPoolExecutor
            ExecutorService executor = Executors.newSingleThreadExecutor();
            Future<?> future = executor.submit(() -> {
                try {
                    Thread.sleep(5000);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                if (progressDialog != null && progressDialog.isShowing()) {
                    progressDialog.dismiss();
                    showInternetErrorDialog();
                    cancel(true); // Annuler la tâche AsyncTask
                }
            });
            executor.shutdown();
        }

        private void showInternetErrorDialog() {
            AlertDialog.Builder builder = new AlertDialog.Builder(MainActivity.this);
            builder.setTitle("Erreur de connexion");
            builder.setMessage("Veuillez vous connecter à Internet ou réessayer plus tard.");
            builder.setPositiveButton("OK", new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialog, int which) {
                    // Fermer la boîte de dialogue ou effectuer d'autres actions nécessaires
                    dialog.dismiss();
                }
            });
            builder.create().show();
        }


        @Override
        protected void onPostExecute(String result) {
            if (progressDialog != null && progressDialog.isShowing()) {
                progressDialog.dismiss();
            }

            if (result != null) {
                try {
                    // Parse the JSON response and update the stationList
                    parseJsonResponse(result);

                    updateTextViewContent(selectedTabPosition);
                } catch (JSONException e) {
                    Log.e("FetchDataTask", "Error parsing JSON: " + e.getMessage());
                    e.printStackTrace();
                }
            } else {
                Log.e("FetchDataTask", "Result is null.");
                // Afficher un message d'erreur
                showInternetErrorDialog();
            }
        }


        private void parseJsonResponse(String result) throws JSONException {
            JSONObject responseJson = new JSONObject(result);

            if (responseJson.has("results")) {
                JSONArray records = responseJson.getJSONArray("results");

                for (int i = 0; i < records.length(); i++) {
                    JSONObject record = records.getJSONObject(i);

                    // Check if the key "adresse" exists in each record
                    if (record.has("adresse")) {
                        String addresse = record.getString("adresse");
                        String ville = record.getString("ville");
                        //String services = record.getString("services");
                        String services = record.optString("services", null);


                        String address = addresse + " " + ville;
                        double gazole_prix = record.optDouble("gazole_prix");
                        double sp95_prix = record.optDouble("sp95_prix", 0.0);
                        double e85_prix = record.optDouble("e85_prix", 0.0);
                        double gplc_prix = record.optDouble("gplc_prix", 0.0);
                        double e10_prix = record.optDouble("e10_prix", 0.0);
                        double sp98_prix = record.optDouble("sp98_prix", 0.0);


                        if (record.has("geom")) {
                            Log.d("FetchDataTask", "Record has 'geometry'");

                            JSONObject geom = record.getJSONObject("geom");

                            if (geom.has("lon") && geom.has("lat")) {
                                Log.d("FetchDataTask", "Record has 'longitude' and 'latitude'");


                                double latitude = geom.getDouble("lat");
                                double longitude = geom.getDouble("lon");

                                Station station = new Station(address, latitude, longitude, gazole_prix, sp95_prix, e85_prix, gplc_prix, e10_prix, sp98_prix, services);


                                // Add the station to the stationList
                                stationList.add(station);
                                //Log.d("FetchDataTask", "Station"+station.getServices());


                            } else {
                                Log.d("FetchDataTask", "Record does not have 'coordinates'");
                            }
                        } else {
                            Log.d("FetchDataTask", "Record does not have 'geometry'");
                        }


                    } else {
                        Log.e("FetchDataTask", "Key 'adresse' not found in the record.");
                    }
                }
            } else {
                Log.e("FetchDataTask", "Key 'results' not found in the JSON response.");
            }
        }

    }
}

